import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Authors, AuthorsPage } from '../employee/author';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getAuthors(page: number, size: number): Observable<AuthorsPage> {
    const authorPage = this.httpClient.get<AuthorsPage>("http://192.168.1.106:8081/api/authors");
    return authorPage
  }

  public deleteAuthors(author) {
    return this.httpClient.delete<Authors>("http://192.168.1.106:8081/api/authors" + "/" + author.id);
  }

  public createAuthors(author: Authors): Observable<Authors> {
    return this.httpClient.post<Authors>("http://192.168.1.106:8081/api/authors", author);
  }

  public updateAuthors(author: Authors): Observable<Authors> {
    return this.httpClient.put<Authors>("http://192.168.1.106:8081/api/authors" + "/" + author.id, author);

  }
}