import { Component, OnInit } from '@angular/core';
import { HttpClientService, } from '../service/httpclient.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Authors } from '../employee/author';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  author: Authors = new Authors();

  constructor(
    private httpClientService: HttpClientService
  ) { }

  submitted = false;

  ngOnInit() {
    this.submitted = false;
  }

  authorSaveForm = new FormGroup({
    authorName: new FormControl('', [Validators.required]),
    dateBirth: new FormControl('', [Validators.required]),
    country: new FormControl('', [Validators.required]),
  });

  saveAuthor(saveAuthor) {
    this.author = new Authors();
    this.author.authorName = this.authorName.value;
    this.author.dateBirth = this.dateBirth.value;
    this.author.country = this.country.value;
    this.submitted = true;
    this.save();
  }



  save() {
    this.httpClientService.createAuthors(this.author)
      .subscribe(data => console.log(data), error => console.log(error));
    this.author = new Authors();
  }

  get authorName() {
    return this.authorSaveForm.get('authorName');
  }

  get dateBirth() {
    return this.authorSaveForm.get('dateBirth');
  }
  get country() {
    return this.authorSaveForm.get('country');
  }

  addAuthorForm() {
    this.submitted = false;
    this.authorSaveForm.reset();
  }

}