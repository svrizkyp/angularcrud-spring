import { Component, OnInit } from '@angular/core';
import { HttpClientService, } from '../service/httpclient.service';
import { Authors } from './author';
import { Observable, Subject } from "rxjs";
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  authors: Authors[];
  authorList: any;
  author: Authors = new Authors();
  isEdit: false;
  constructor(
    private httpClientService: HttpClientService
  ) { }

  ngOnInit() {
    this.getAuthor();
    isEdit: false;
  }
  handleSuccessfulResponse(response) {
    this.authors = response;
  }

  deleteAuthor(author: Authors): void {
    this.httpClientService.deleteAuthors(author)
      .subscribe(data => {
        this.authors = this.authors.filter(u => u !== author);
      })
  };

  getAuthor() {
    this.httpClientService.getAuthors(0, 5).subscribe(authorPage => {
      this.authors = authorPage.content;
      // console.log(JSON.stringify(this.authors))
    });
  }

  EditAuthor(author) {
    author.isEdit = true;
    author.EditId = author.id;
    author.EditName = author.authorName;
    author.EditBirth = author.dateBirth;
    author.EditCountry = author.country;
  }

  UpdateAuthor(author) {
    let authors = {};
    authors['id'] = author.EditId;
    authors['authorName'] = author.EditName;
    authors['dateBirth'] = author.EditBirth;
    authors['country'] = author.EditCountry;
    this.httpClientService.updateAuthors(this.author)
      .subscribe(data => console.log(data), error => console.log(error));
    this.author = new Authors();
    author.isEdit = false;
    console.log(JSON.stringify(this.authors));
  }




}