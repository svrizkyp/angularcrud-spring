export class Authors{
      public id:number;
      public authorName:string;
      public dateBirth: Date;
      public country:string;
  };
  
export class AuthorsPage {
    content: Authors[];
    last: boolean;
    totalElements: number;
    totalPages: number;
    size: number;
    number: number;
    }